#!/usr/bin/env bash
#
#===============================================================================
#                IonApp
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /start.sh
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: A Script to get the Runtime environment downloaded and ready.
#
#-----------------------------------------------------------------------------
#
#===============================================================================

#
# Where the app will be deployed. The runtime will be located there

IONAPP_HOME=/srv/ionapp


###[ You probably don't need to change anything below this line ]###

# JRuby Version (see also: runtime.sh)
JRUBY_VER="9.3.2.0"
JRUBY_TAR="jruby-dist-${JRUBY_VER}-bin.tar.gz"
JRUBY_URL="https://repo1.maven.org/maven2/org/jruby/jruby-dist/${JRUBY_VER}/jruby-dist-${JRUBY_VER}-bin.tar.gz"

# OpenJDK Version & Binary location (see also: runtime.sh)
JDK_VER="17.0.1"

# Get the host OS
OSNAME=`uname`
OSARCH=`uname -m`

echo "IonApp - Fetching runtime components into $IONAPP_HOME"

echo "Detected $OSNAME on $OSARCH"

if [ $OSARCH != "x86_64" ]
then
  echo "This script prepares a runtime for MacoS or Linux x64 machines. This system is not supported by this tool. "
  exit 1
fi

case "$OSNAME" in
  "Darwin")
    JDK_TAR="openjdk-17.0.1_osx-x64_bin.tar.gz"
    JDK_URL="https://download.java.net/java/GA/jdk17.0.1/2a2082e5a09d4267845be086888add4f/12/GPL/openjdk-17.0.1_macos-x64_bin.tar.gz"
  ;;
  "Linux")
    JDK_TAR="openjdk-17.0.1_linux-x64_bin.tar.gz"
    JDK_URL="https://download.java.net/java/GA/jdk17.0.1/2a2082e5a09d4267845be086888add4f/12/GPL/openjdk-17.0.1_linux-x64_bin.tar.gz"
  ;;
esac


### PREPARE THE RUNTIME ###

THISUSER=`whoami`

if [ -d ${IONAPP_HOME} ]
then
  echo "Target Directory already exists"
else
  echo "Creating target"
fi

echo "Creating '${IONAPP_HOME}' directory"
if mkdir -p ${IONAPP_HOME} 
then
  echo "Created!"
else
  echo "Unable to create target directory. Try this command to fix: "
  echo " sudo mkdir -p ${IONAPP_HOME} && sudo chown -R ${THISUSER}: ${IONAPP_HOME}"
  echo ""
  echo "Alternatively, create a directory in your homedir and symlink"
  echo " mkdir ~/srv && sudo ln -s ~/srv /srv"
  exit 1
fi

echo "Entering '${IONAPP_HOME}'"
cd ${IONAPP_HOME}

echo "Creating 'runtime' directory"
mkdir runtime
echo "Entering 'runtime' directory"
cd runtime

echo "installing JDK from ${JDK_URL}"
wget ${JDK_URL}
tar -xzvf ${JDK_TAR}
rm ${JDK_TAR}
# Extra step on MacOS to fix runtime pathname
if [ $OSNAME == "Darwin" ]
then
  ln -s jdk-13.jdk/Contents/Home/ jdk-13
fi

echo "installing JRuby from ${JRUBY_URL}"
wget ${JRUBY_URL}
tar -xzvf ${JRUBY_TAR}
rm ${JRUBY_TAR}

echo "preparing additional directories"
if [ -d ${IONAPP_HOME}/conf ]
then
  echo "'conf' directory already exists, skipping."
else
  echo "Creating 'conf' directory"
  mkdir -p ${IONAPP_HOME}/conf
fi

if [ -d ${IONAPP_HOME}/log ]
then
  echo "'log' directory already exists, skipping."
else
  echo "Creating 'log' directory"
  mkdir -p ${IONAPP_HOME}/log
fi

echo "done!"
exit 0
