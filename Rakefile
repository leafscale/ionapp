=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: Rakefile
# Description: Tasks for working with IonApp framework.
#
#-----------------------------------------------------------------------------
=end

require 'rake'
require 'rdoc/task'
$LOAD_PATH.unshift('.')
require 'java'
require 'lib/ionconstants'
require 'lib/ionio'
    
task :default do
  desc "IonApp - rake help"
  puts "IonApp uses ruby's rake facility to automate specific tasks."
  puts "usage:  rake [ns:task]\n"
  puts "\n"
  puts "Configuration"
  puts "  conf:default      - Create a default config file sample"
  puts "  conf:session      - Generate the session secret"
  puts "\n"
  puts "Documentation"
  puts "  doc:rdoc          - Build the API documentation"
  puts "\n"
  puts "Database"
#  puts "  db:viewconf       - View database configuration"
  puts "  db:testconnect    - Test connection to database"
  puts "  db:migrate        - Run Sequel DB migration tasks"
  puts "  db:demigrate      - Run Sequel DB demigrate tasks"
  puts "  db:preload        - Inserts initial datasets if empty"
  puts ""
end

#==============================================================================
# Documentation Namespace
#==============================================================================

namespace :doc do
  desc "Create documentation"
  Rake::RDocTask.new do |rdoc|
        files = ['README', 'LICENSE',
                 'lib/**/*.rb', 'doc/**/*.rdoc']
        rdoc.rdoc_files.add(files)
        rdoc.main = 'README'
        rdoc.title = 'IonFalls API rdocs'
        rdoc.rdoc_dir = 'doc/api'
        rdoc.options = [ '--line-numbers' ]
  end
end

#==============================================================================
# Configuration Namespace  
#==============================================================================
namespace :conf do
  desc "Create default configuration file sample"
  task :default do
    require 'lib/ionconfig'
    Ionconfig.makedefault('conf/app.conf.yaml-SAMPLE')
  end

  desc "Generate the session environment script"
  task :session do
    f = File.new('./session.sh', "w+",0640)
    require 'securerandom'; 
    secretkey = SecureRandom.hex(64)
fout = <<-'EOL'
#
# Session Secret for RODA App
# do not delete this file and protect it
#
EOL
    f.puts fout
    f.puts "export SESSION_SECRET=\"#{secretkey}\" " 

    f.close
  end

end

#==============================================================================
# Database Namespace  
#==============================================================================
namespace :db do
  # Need to load the configuration file to access the DB

 
  # ----------------------------------   
  desc "Show Database Configuration"
  task :viewconf do
    require "lib/ionconfig.rb"
    cfg = Ionconfig.load
    require 'lib/iondatabase'
    pp cfg['database']
  end

# Disabled as this was leftover in IonFalls  
#  desc "Reveal Database Password"
#  task :viewsapwd do  
#    dbpwd = Security::ConfigPassword.new.decrypt(cfg['database']['pass'])
#    puts dbpwd
#  end  
    
  desc "Test Database Connection"
  task :testconnect do
    require "lib/ionconfig.rb"
    cfg = Ionconfig.load
    require 'lib/iondatabase'

      begin
      Ionio.printstart "Testing database connection"
        dbconn = Iondatabase.connect(cfg['database'])
      Ionio.printreturn(0)
    rescue Exception => e
      Ionio.printreturn(1)
        puts e
      exit 1
    end
  end
  
  desc "Run migrations"
  task :migrate, [:version] => [:testconnect] do |t, args|
    require "lib/ionconfig.rb"
    cfg = Ionconfig.load
    require 'lib/iondatabase'

    Sequel.extension :migration
    version = args[:version].to_i if args[:version]
    begin
      Ionio.printstart "Running Sequel migrations on database"
        dbconn = Iondatabase.connect(cfg['database'])
        Sequel::Migrator.run(dbconn, "db/migrations", target: version)    
      Ionio.printreturn(0)
    rescue Exception => e
      Ionio.printreturn(1)
        puts e
      exit 1
    end
  end

  desc "Run de-migrations"
  task demigrate: [:testconnect] do
    require "lib/ionconfig.rb"
    cfg = Ionconfig.load
    require 'lib/iondatabase'

    Sequel.extension :migration
    begin
      Ionio.printstart "Running Sequel migration to version 0 on database"
        dbconn = Iondatabase.connect(cfg['database'])
        Sequel::Migrator.run(dbconn, "db/migrations", target: 0)    
      Ionio.printreturn(0)
    rescue Exception => e
      Ionio.printreturn(1)
        puts e
      exit 1
    end
  end

  desc "Run database preload"
  task preload: [:testconnect] do
    require "lib/ionconfig.rb"
    cfg = Ionconfig  
    require 'lib/iondatabase'

      require 'models/iondatabase_models'
      load 'models/iondatabase_preload.rb'
  end
end


#==============================================================================
