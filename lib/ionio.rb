=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionio.rb
#     Version: 0.06
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Input Output Handlers
#
#-----------------------------------------------------------------------------
=end

require 'io/console'
require 'rubygems'

=begin rdoc               
= Ionio (I/O)
Ionio provides the common input/output routines for dealing with STDOUT and STDIN. This includes the ANSI color output methods, pauses, screenclear, etc.
=end


module Ionio
 
  # rb input using 'io/console' library
  # What this does is uses the RUBY built in of getch, which itself returns a string.
  # We need to convert this to an ASCII keycode, which is done by converting it to a
  # java string, then java character array, and getting the first value.
  def Ionio::rbinput
    return IO.console.getch.to_java.toCharArray[0]
  end

    # calls one of the other input methods to be easily interchangeable for testing
  def Ionio::rawinput
   self.rbinput
  end
 
  # Print Output in a ANSI color block using Term::ANSIColor
  def Ionio::ansiprint(text, fgcolor=ANSI_WHITE, bgcolor=ANSI_ON_BLACK)
      print fgcolor+bgcolor+text+ANSI_RESET
  end #/ansiprint

  # Display the program's MAIN banner
  def Ionio.mainbanner
    ansiprint("IonApp -  https://www.ionfalls.org", ANSI_BRIGHT_CYAN)
    ansiprint(" v", ANSI_CYAN)
    ansiprint(APP_VERSION, ANSI_BRIGHT_CYAN)
    ansiprint(" :: ", ANSI_BLUE)
    ansiprint("Copyright (c) 2008-2019, Chris Tusa\n", ANSI_BRIGHT_CYAN)
    ansiprint("All rights reserved.\n", ANSI_CYAN)
    ansiprint("Refer to the LICENSE file.\n\n", ANSI_DARK_GRAY)
  end #/def mainbanner


  # The desired effect is to clear the screen
  def Ionio::ansiclear
    puts %x{clear}
  end #/def ansiclear

  # Print to stdout that an item is starting
  def Ionio.printstart(txt)
    ansiprint(txt, ANSI_WHITE)
    ansiprint("."*(60-txt.length), ANSI_GRAY)
  end #/def printstart

  # Print to stdout the result
  def Ionio.printreturn(retval)
    ansiprint("[", ANSI_BLUE)
    if retval == 0
      ansiprint("DONE", ANSI_BRIGHT_CYAN)
    elsif retval == 1
      ansiprint("FAIL", ANSI_BRIGHT_RED)
    elsif retval == 2
      ansiprint("WAIT", ANSI_CYAN)
    elsif retval == 3
      ansiprint("STRT", ANSI_BRIGHT_GREEN)
    else
      ansiprint("????", ANSI_BRIGHT_MAGENTA)
    end
    ansiprint("]\n", ANSI_BLUE)
  end #/def printreturn


end #/module
