=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: lib/iondatabase.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Database Connectivity
#
#-----------------------------------------------------------------------------
=end

=begin rdoc               
= Iondatabase (Database)
Iondatabase provides the database connectivity and initialization routines.

== Embedded
IonFalls includes an embedded database engine. H2 is the included database, 
and is provided as a JAR file with the distribution. 
see (http://h2database.com).

=end

module Iondatabase
  require 'rubygems'
  require 'sequel'
#  Sequel::Model.plugin(:schema)  # This plugin was depreacted in gem Sequel >= v4.5
  Sequel::Model.plugin(:skip_create_refresh)
  Sequel::Model.plugin(:validation_helpers)
  Sequel::Model.plugin(:json_serializer)
  
  # Create Database Connection based on type of either H2 Embedded or External Remote.
  def Iondatabase.connect(db)
      # -> Try to connect to H2 Database from Sequel.
      begin
        if db['mode'] == 'remote' then
          Sequel.connect("jdbc:h2:tcp://localhost/#{db['name']}", :user => "#{db['user']}", :password=> "#{db['password']}")
        else
          Sequel.connect("jdbc:h2:./db/#{db['name']}", :user => "#{db['user']}", :password=> "#{db['password']}")
        end
      rescue Sequel::DatabaseConnectionError => e
        raise "ERROR: Unable to connect to embedded database. (#{e})"
      rescue Java::NativeException => e
        raise "ERROR: Unable to connect to embedded database. (#{e})"
      end
  end #/def connect


  # => Initialize the database (reserved)
  def Iondatabase.initdb
    # The database is initialized using Sequel migrations (see Rakefile)
  end #/dev init

end # /module
