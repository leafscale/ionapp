=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionloader.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Loader for the library units
#
#-----------------------------------------------------------------------------
=end

=begin rdoc               
= Ionloader (Ionloader)
Ionloader is the main Ionloader library responsible for initialization of sub libraries.
=end

module Ionloader

# Load the constants definition
def Ionloader.loadlib_ionconstants
  begin
    require 'lib/ionconstants'
  rescue LoadError
    puts "FATAL: cannot load library 'ionconstants'."
    exit 1
  end
end

# Load Library: Security
def Ionloader.loadlib_security
  begin
    require 'lib/security'
  rescue LoadError
    puts "FATAL: cannot load library 'security'."
    exit 1
  end
end


# Load Library: Ionio
def Ionloader.loadlib_ionio
  begin
    require 'lib/ionio'
    Ionio.mainbanner
  rescue LoadError
    puts "FATAL: cannot load library 'ionio'."
    exit 1
  end
end

# Load Library: Ionconfig
def Ionloader.loadlib_ionconfig
  begin
    require 'lib/ionconfig'
  rescue LoadError
    puts "FATAL: cannot load library 'ionconfig'."
    exit 1
  end
end

# Loads configuration into a global $cfg variable
def Ionloader.load_ionconfig
  begin
    Ionio.printstart "Loading Ion configuration"
    $cfg = Ionconfig.load
    Ionio.printreturn(0)
  rescue Exception => e
    Ionio.printreturn(1)
    Ionconfig.makedefault('./conf/app.conf.yaml.sample')
    puts e
    exit 1
  end
end

# Load Library: Iondatabase
def Ionloader.loadlib_iondatabase
  begin
    require 'lib/iondatabase'
  rescue LoadError
    puts "FATAL: cannot load library 'iondatabase'."
    exit 1
  end
end

# Connect to the user specified DB engine & load the Sequel models
def Ionloader.load_databaseconn
  begin
    Ionio.printstart "Initializing database connection"
    $db = Iondatabase.connect($cfg['database'])
    Ionio.printreturn(0)
  rescue Exception => e
    Ionio.printreturn(1)
    puts e
    exit 1
  end
end

# Load Library: Iondatabase_models
def Ionloader.loadlib_iondatabasemodels
  Ionio.printstart "Initializing Database Models"
  Ionio.printreturn(2)
  begin
    require 'lib/ionmodel'
    require 'models/iondatabase_models'
  rescue LoadError
    puts "FATAL: cannot load library 'iondatabase_models'."
    exit 1
  rescue Sequel::DatabaseConnectionError => e
    puts "Unable to connect to database: #{e}"
    exit 1
  end
  Ionio.printstart "Database Models Initialized OK"
  Ionio.printreturn(0)
end

=begin
# Load Library: Iontemplate
def Ionloader.loadlib_iontemplate
  begin
    Ionio.printstart "Loading Template Engine"
    require 'lib/iontemplate'
    Ionio.printreturn(0)
  rescue LoadError    
    puts "FATAL: cannot load library 'iontemplate'."
    Ionio.printreturn(1)
    exit 1
  end
end 
=end

=begin
# Load Library: Ioncontroller
def Ionloader.loadlib_ioncontroller
  begin
    Ionio.printstart "Loading Master Controller"
    require 'lib/ioncontroller'
    Ionio.printreturn(0)
  rescue LoadError
    puts "FATAL: cannot load library 'ioncontroller'."
    Ionio.printreturn(1)
    exit 1
  end
end
=end

# Load the entire stack in the correct order. Use this method for anything that requires full init
def Ionloader.init
  self.loadlib_ionconstants
  self.loadlib_ionio
  self.loadlib_ionconfig
  self.load_ionconfig
  self.loadlib_iondatabase
  self.load_databaseconn
  self.loadlib_iondatabasemodels
#  self.loadlib_iontemplate
#  self.loadlib_ioncontroller
end


end #/module Ionloader
