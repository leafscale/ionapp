=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionconfig.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Configuration Handlers
#
#-----------------------------------------------------------------------------
=end

=begin rdoc               
= Ionconfig (Configuration)
Ionconfig handles configuration file management Configuration is stored in YAML syntax formatting.
=end


module Ionconfig
  require 'yaml'
  require 'rubygems'

  Configfile = "./conf/app.conf.yaml"

  # Builds a default YAML configuration and outputs the filename specified.
  def Ionconfig.makedefault(filename)
    cfghash = {
            "app" => {
                    "name"    => "IonApp New Application",
                    "tagline" => "",
                    "enabled" => true
            },
            "database" => {
                    "host"    => "localhost",
                    "name"    => "ionappdb",
                    "user"    => "sa",
                    "password"=> "changeme"
            }    
    }
    f = File.open(filename, "w")
    f.puts YAML::dump(cfghash)
    f.close
  end

  # Load in the YAML configuration file and returns
  def Ionconfig.load
    cfg = File.open(Configfile)  { |yf| YAML::load( yf ) }
    # => Ensure loaded data is a hash. ie: YAML load was OK
    if cfg.class != Hash
       raise "ERROR: Ionconfig - invalid format or parsing error."
    # => If all is well, perform deeper validation
    else
      # => PARSE & CHECK: Database Section
      # TODO: Need to expand the checks
      if cfg['database'].nil?
        raise "ERROR: Ionconfig - database section not defined."
      end #-> /Parse DB

      # => PARSE & CHECK: BBS Section
      if cfg['app'].nil?
        raise "ERROR: Ionconfig - bbs section not defined."
      end #-> /Parse BBS
    end #-> /if !Hash

    # => If all is well, return the configuration
    return cfg
  end #/def

  # Save changes to the YAML file with the hash passed in.
  def Ionconfig.save(cfghash)    
    f = File.open(Configfile, "w")
    f.puts YAML::dump(cfghash)
    f.close
  end  
  
end # => /module
#EOL
