=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /db/ionconstants.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Program constant definitions.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc               
= Ionconstants (Global Constants)
Ionconstants defines all of the program's constants and Java Class imports.
=end


# Version Information
APP_VERSION="0.1".freeze
VERSION_IS_STABLE=false

# ANSI COLORS DEFINITIONS (not Term::ANSIColor)
ANSI_RESET     = "\e[0m".freeze
ANSI_BOLD      = "\e[1m".freeze
ANSI_DARK      = "\e[2m".freeze
ANSI_ITALIC    = "\e[3m".freeze
ANSI_UNDERLINE = "\e[4m".freeze
ANSI_BLINK     = "\e[5m".freeze
ANSI_RAPID     = "\e[6m".freeze
ANSI_NEGATIVE  = "\e[7m".freeze
ANSI_CONCEALED = "\e[8m".freeze
ANSI_STRIKE    = "\e[9m".freeze

# -> BASE COLORS
ANSI_BLACK     = "\e[0;30m".freeze
ANSI_RED       = "\e[0;31m".freeze
ANSI_GREEN     = "\e[0;32m".freeze
ANSI_YELLOW    = "\e[0;33m".freeze
ANSI_BLUE      = "\e[0;34m".freeze
ANSI_MAGENTA   = "\e[0;35m".freeze
ANSI_CYAN      = "\e[0;36m".freeze
ANSI_GRAY      = "\e[0;37m".freeze

# -> BACKGROUND COLORS
ANSI_ON_BLACK  = "\e[40m".freeze
ANSI_ON_RED    = "\e[41m".freeze
ANSI_ON_GREEN  = "\e[42m".freeze
ANSI_ON_YELLOW = "\e[43m".freeze
ANSI_ON_BLUE   = "\e[44m".freeze
ANSI_ON_MAGENTA= "\e[45m".freeze
ANSI_ON_CYAN   = "\e[46m".freeze
ANSI_ON_WHITE  = "\e[47m".freeze

# -> BRIGHTENED BASE COLORS
ANSI_DARK_GRAY      = "\e[1;30m".freeze
ANSI_BRIGHT_RED     = "\e[1;31m".freeze
ANSI_BRIGHT_GREEN   = "\e[1;32m".freeze
ANSI_BRIGHT_YELLOW  = "\e[1;33m".freeze
ANSI_BRIGHT_BLUE    = "\e[1;34m".freeze
ANSI_BRIGHT_MAGENTA = "\e[1;35m".freeze
ANSI_BRIGHT_CYAN    = "\e[1;36m".freeze
ANSI_WHITE          = "\e[1;37m".freeze
ANSI_BRIGHT_WHITE   = ANSI_WHITE

# -> CLEAR SCREEN
ANSI_CLS       = "\e[2J\e[H".freeze

# Defines the full list of ANSI codes as a Hash for use elsewhere
ANSICOLORS = {
   "cls"          => ANSI_CLS,
   "reset"        => ANSI_RESET,
   "norm"         => ANSI_RESET,
   "bold"         => ANSI_BOLD,
   "dark"         => ANSI_DARK,
   "italic"       => ANSI_ITALIC,
   "underline"    => ANSI_UNDERLINE,
   "blink"        => ANSI_BLINK,
   "rapid"        => ANSI_RAPID,
   "negative"     => ANSI_NEGATIVE,
   "concealed"    => ANSI_CONCEALED,
   "strike"       => ANSI_STRIKE,
   "black"        => ANSI_BLACK,
   "red"          => ANSI_RED,
   "green"        => ANSI_GREEN,
   "yellow"       => ANSI_YELLOW,
   "blue"         => ANSI_BLUE,
   "magenta"      => ANSI_MAGENTA,
   "cyan"         => ANSI_CYAN,
   "gray"         => ANSI_GRAY,
   "grey"         => ANSI_GRAY,
   "on_black"     => ANSI_ON_BLACK,
   "on_red"       => ANSI_ON_RED,
   "on_green"     => ANSI_ON_GREEN,
   "on_yellow"    => ANSI_ON_YELLOW,
   "on_blue"      => ANSI_ON_BLUE,
   "on_magenta"   => ANSI_ON_MAGENTA,
   "on_cyan"      => ANSI_ON_CYAN,
   "on_white"     => ANSI_ON_WHITE,
   "lightred"     => ANSI_BRIGHT_RED,
   "lightgreen"   => ANSI_BRIGHT_GREEN,
   "lightyellow"  => ANSI_BRIGHT_YELLOW,
   "lightblue"    => ANSI_BRIGHT_BLUE,
   "lightmagenta" => ANSI_BRIGHT_MAGENTA,
   "lightcyan"    => ANSI_BRIGHT_CYAN,
   "white"        => ANSI_WHITE,
   "brightred"    => ANSI_BRIGHT_RED,
   "brightgreen"  => ANSI_BRIGHT_GREEN,
   "brightyellow" => ANSI_BRIGHT_YELLOW,
   "brightblue"   => ANSI_BRIGHT_BLUE,
   "brightmagenta"=> ANSI_BRIGHT_MAGENTA,
   "brightcyan"   => ANSI_BRIGHT_CYAN,
   "brightwhite"  => ANSI_WHITE,
   "darkblack"    => ANSI_BLACK,
   "darkred"      => ANSI_RED,
   "darkgreen"    => ANSI_GREEN,
   "darkyellow"   => ANSI_YELLOW,
   "darkblue"     => ANSI_BLUE,
   "darkmagenta"  => ANSI_MAGENTA,
   "darkcyan"     => ANSI_CYAN,
   "darkgray"     => ANSI_DARK_GRAY,
   "darkgrey"     => ANSI_DARK_GRAY,
   "brown"        => ANSI_YELLOW,
}.freeze


# Extend the Ruby Classes using IonFalls overrides
require 'lib/rubyclass_extensions'
