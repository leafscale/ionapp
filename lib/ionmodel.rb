=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionmodel.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Class methods and overrides for Sequel::Model

#-----------------------------------------------------------------------------

=begin rdoc               
= ionmodel Class extensions for Sequel
Overrides for the Sequel::Model to provide hooks or other modifications.
=end

class Sequel::Model
  def before_create
    self.created ||= Time.now
    super
  end

  def before_save
    self.modified ||= Time.now
    super
  end

end
