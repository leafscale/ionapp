#!/bin/bash

IONAPP_HOME=/srv/ionapp
JAVA_VER=17.0.1
JRUBY_VER=9.3.2.0
# Set the environment 'production' or 'development'
PUMA_ENV="development"

export JAVA_HOME=$IONAPP_HOME/runtime/jdk-$JAVA_VER
export JRUBY_HOME=$IONAPP_HOME/runtime/jruby-$JRUBY_VER
export PATH=$JAVA_HOME/bin:$JRUBY_HOME/bin:$PATH
