=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: app.rb
# Description: Main Roda setup and routing file.
#
#-----------------------------------------------------------------------------
=end

$LOAD_PATH.unshift('.')
require 'java'
require 'lib/ionloader'

Ionloader.init

require 'bcrypt'
require 'roda'
require 'roda/session_middleware'
require 'rack'
require 'rack/protection'
require 'logger'

class IonApp < Roda
#  use Rack::Session::Cookie, secret: "o9PsjbVty2ja8hra0xiSInF0AxXiSTrRoA4a09vv3aEGKUFH", key: "_ionapp_session"
  use RodaSessionMiddleware, secret: ENV['SESSION_SECRET'], key: "_ionapp_session"
  use Rack::Protection
  
  plugin :common_logger, Logger.new('log/ionapp.log')
  plugin :static, ["/images", "/css", "/js", "/html"]
  plugin :flash
  plugin :render
  plugin :head
  plugin :csrf
  plugin :json, include_request: true, classes: [Array, Hash, Sequel::Model]


# Parses the POST data for <select multiple> as 
# the 'parse_nested_query' deduplicates the values
# This method requires passing in the Request, and
# namespace of the form. Returns a hash
  def readpost(req, namespace)
    data = Hash.new
    Rack::Utils.parse_query(req.env['rack.input'].read).each do |i|
      if i[0].start_with?(namespace)
        #puts "Found Namespace #{namespace} item #{i[0]}, parsing value of #{i[1]}"
        hkey = i[0].split('[')[1].delete(']')
        hval = i[1]
        data.store(hkey, hval)
      end
    end
    #return { namespace => data }
    return data
  end #/readpost


#============================================================================
#  Route: START ROUTING HERE
#============================================================================
route do |r|

  if session[:user_id].nil?
    # NO SESSION FOUND
    @validuser = false
  else
    # FOUND VALID SESSION
    @validuser = true
    if User.where(:id => session[:user_id]).first[:is_admin] == true
      @adminuser = true
    end   
  end


#============================================================================
#  Route: /
#============================================================================
  r.root do
    @announcements = Announcement.all
    view("startpage")
  end # /root


#============================================================================
#  Route: /about STATIC PAGE
#============================================================================
  r.get "about" do
    view("about")
  end


#============================================================================
#  Route: /ionadmin
#============================================================================
  r.on "ionadmin" do
    unless @adminuser == true
      r.redirect "/ionlogin"
    else
      autoforme(:admin)
    end  
  end


#============================================================================
#  Route: /login /logout
#============================================================================
  r.get "ionlogin" do
    view("login")
  end

  r.post "ionlogin" do
    if user = User.authenticate(r["login"], r["password"])
      session[:user_id] = user.id
      #puts "AUTH SUCCESS FOR: #{r["login"]}"
      r.redirect "/"
    else
      #puts "AUTH FAILURE FOR: #{r["login"]}"
      r.redirect "/login"
    end
  end

  r.post "ionlogout" do
    session.clear
    r.redirect "/"
  end

  r.get "ionlogout" do
    view("logout")
  end


#============================================================================
#  Route: /users
#============================================================================
  r.on "ionusers" do
     unless @adminuser == true
      r.redirect "/"
     end
     
    # NEW User 
    r.get "new" do
      @user = User.new
      view("users/new")
    end
      
    # SHOW user by login username
    r.on "username", String, method: :get do |username|
      @user = User.find_by_login(username)
      view("users/show")        
    end
    # INDEX list all users
    r.is do
      r.get do
         @users = User.all
         view("users/index")
      end
      
      # Update user
      r.post do
        @user = User.new(r["user"])
        if @user.valid? && @user.save
          r.redirect "/users"
        else
          view("users/new")
        end
      end #/ post-users
    end #/is
  end #/users    


#============================================================================
#  Route: /announcements
#============================================================================
  r.on "announcements" do
    @news = Announcement.titleshash
    @announcements = Announcement.all 
    # NEW Announcement
    r.on "new" do
      unless @adminuser == true
       r.redirect "/announcements"
      else        
        @announcement = Announcement.new
        view("announcements/new")
      end  
    end
      
    # SHOW announcement by name
    r.on "details", String, method: :get do |id|
      @announcement = Announcement.where(:id => id).first
      view("announcements/show")        
    end

    # GET /announcements/edit/<id>
    r.on "edit", String do |id|        
      unless @adminuser == true
       r.redirect "/announcements"
      end
      @announcement = Announcement.where(:id => id).first
       r.get do
        view("announcements/new")
      end
       r.post do
        if @announcement.update(r["announcement"])
          r.redirect "/announcements/details/#{@announcement.id}"
        else
          view("announcements/new")
        end
      end
    end #/edit

    # GET /announcements/delete/<id>
    r.on "delete", String do |id|        
      unless @adminuser == true
       r.redirect "/announcements"
      end
      @announcement = Announcement.where(:id => id).first
      r.get do
        view("announcements/delete")
      end
    end #/delete
    # GET /announcements/confirmdelete
    r.on "confirmdelete", String do |id|
      r.get do
        Announcement.where(:id => id).delete
        flash[:success] = 'Deleted hardware category.'
        r.redirect "/announcements"
      end    
    end #/confirmdel
    
    # INDEX list all announcement
    r.is do
      r.get do
         view("announcements/index")
      end
      # TODO: Refactor this - Post new system
      r.post do
        @announcement = Announcement.new(r["announcement"])
        if @announcement.valid? && @announcement.save
          r.redirect "/announcements"
        else
          view("announcements/new")
        end
      end #/post-announcement
    end #/is
    
  end #/on announcements


#============================================================================
#  Route: END ROUTING
#============================================================================
end #/route
#============================================================================
#  APP END
#============================================================================
end #/app
