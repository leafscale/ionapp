# IonApp #

### What is this? ###
IonApp is a MVC web-application framework designed for specifically for JRuby. Leveraging a combination of Ruby Gems and Java Jar files, IonApp introduces a flexible way to take advantage of the ecoystem of two powerful programming languages within a single framework. 

#### Current Version = 0.1 ####
The IonApp framework was extracted from the IonFalls BBS project's library and modified to run web applications. The library itself has been written for several years. Combining the IonFalls pieces with the brilliant work of Jeremey Evan's Roda and Sequel gems created a rapid prototype for a useable web-application framework.

### What components are used? ###
* OpenJDK (https://openjdk.java.net)
* JRuby (http://jruby.org)
* Roda (http://roda.jeremyevans.net)
* Sequel (http://sequel.jeremevans.net)
* H2 Database (http://h2database.com)
* Puma Webserver (https://puma.io/)

### About Dependencies / Runtime ###
IonApp does not ship the runtime components due to license differences. 
OpenJDK and JRuby binaries are required in order to develop and run the software.

A script, 'getruntime.sh' is provided to assist in deploying these for you. 

The runtime and application will default and expect to be located in /srv/ionapp. If you want to change this, you will have to modify the IONAPP_HOME variable in two files:
'runtime.sh' and 'getruntime.sh'
Modify  and change:
  IONAPP_HOME=/srv/ionapp

### Getting Started ###
These instructions assume you are setting up new development environment in ' /srv/ionapp ' on a Linux (x64/AMD64) host. Configuration on MacOS is possible and has been tested, but will require minor changes to the 'getruntime.sh' and 'runtime.sh' files. This code has not been tested in Windows or other operating systems.

#### Install Components ####
Clone the repository using git, then use the getruntime.sh script to download the binary runtime components.

* chmod a+x getruntime.sh
* ./getruntime.sh
* cp * /srv/ionapp
* cd /srv/ionapp
* source ./runtime.sh
* jgem install bundler
* bundler

You should now have a complete and working runtime. 

#### Configuration Files ####
Create the default configuration file, copy the sample, and edit the settings.

* rake conf:default 
* cp conf/app.conf.yaml-SAMPLE conf/app.conf.yaml 

Edit the settings in 'conf/app.conf.yaml', particularly the database password. The default is 'changeme'.  

It is also time to create the unique session identifier for your app. We have a rake task for that:

* rake conf:session

#### Setup the Database ####
The last step to getting a working development environment is to create the database. 
IonApp uses database migrations to manage the schema. A sample Users & Announcements 
model are created by default, and a starting set of entries are inserted to verify 
functioning.  You can modify or delete this data at a later date. 

* rake db:migrate
* rake db:preload


#### Tesing the setup ####
Now the server should start:

* ./start

Point a browser to to port 9292

http://127.0.0.1:9292
