=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: bbs/debugshell.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Loads a 'jirb' session with the Telegard environment.
#
#-----------------------------------------------------------------------------

=end

# => Check if the program was called using the correct Ruby Interpreter
if RUBY_VERSION >= "2.5.0"
  unless defined? JRUBY_VERSION
    puts "IonFalls requires JRuby. Please visit ( http://www.jruby.org/ )."
    exit 255
  end
else
  puts "IonFalls requires JRuby interpreter compatible with Ruby '2.5.0' or later."
end

$LOAD_PATH.unshift('.')

# Detect Platform information and store into hash.
require 'java'
system        = java.lang.System
$jvminfo = {
'os_type'      => system.getProperty('os.name'),
'os_arch'      => system.getProperty('os.arch'),
'os_vers'      => system.getProperty('os.version'),
'java_vend'    => system.getProperty('java.vendor'),
'java_vers'    => system.getProperty('java.version'),
'java_home'    => system.getProperty('java.home'),
'java_vm_vend' => system.getProperty('java.vm.vendor'),
'java_vm_vers' => system.getProperty('java.vm.version'),
'java_vm_name' => system.getProperty('java.vm.name'),
}

require 'lib/ionloader'
Ionloader.init

# Draw a nifty header box
tlc = "\u250c"
trc = "\u2510"
blc = "\u2514"
brc = "\u2518"
hline = "\u2500"
vline = "\u2502"
row1 = tlc + (hline * 77) + trc 
row2 = vline + "IonApp - interactive debugger" + (" " * 46) + vline
row3 = blc + (hline * 77) + brc 
puts row1
puts row2
puts row3
#
puts "system summary:"
pp $jvminfo
puts "\nfor help, please read the docs: https://www.leafscale.com/wiki/"
puts "type 'quit' or press CTRL-D to exit the debug shell\n"
