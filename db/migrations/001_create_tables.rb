=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
# see LICENSE file distributed for copyright information
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: db/migrations/001_create_tables.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Database migration file for creating the table structure.
#
#-----------------------------------------------------------------------------
=end

Sequel.migration do
  change do

#============================================================================
# Table: Users
#============================================================================
  create_table(:users) do
    primary_key  :id
    boolean      :islocked
    String      :login,          size: 16, unique: true
    String      :firstname,      size: 25
    String      :lastname,       size: 25
    String      :password_hash
    String      :city,           size: 25
    String      :state,          size: 5
    String      :country,        size: 5
    String      :email,          size: 60
    String      :pwhint_question
    String      :pwhint_answer
    Date        :pwexpires
    boolean     :is_admin, null: false, default: false
    Fixnum      :login_failures
    Fixnum      :login_total
    TimeStamp   :login_last
    TimeStamp   :created
    TimeStamp   :modified
  end

#============================================================================
# Table: Announcement
#============================================================================
  create_table(:announcements) do
    primary_key  :id
    varchar      :title
    Text         :body
    varchar      :submitted_by
    integer      :priority
    TimeStamp    :created
    TimeStamp    :modified
    boolean      :expires,  :default => false
    Date         :expiration
  end

#============================================================================
  end #// change do
end #// migration do
