=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/iondatabase_models.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Define Database Models. This file can only be called after a
#              Sequel database instance has been created.
#
#-----------------------------------------------------------------------------

=begin rdoc               
= Iondatabase_models (Database Models)
Iondatabase_models defines the Sequel Model classes for the data structures.

== Database Models loader
This file simply loads the database models.  The order matters in some cases
=end
require 'models/user'
require 'models/announcement'

#!EOF
