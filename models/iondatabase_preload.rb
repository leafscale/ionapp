=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /models/iondatabase_preload.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: This file holds the default data for preloading the database
#  tables needed to run a minimal install.
#
#-----------------------------------------------------------------------------

=begin rdoc               
= Database Preloader
the default data for preloading the database tables needed to run a minimal install
=end
require 'bcrypt'

Ionio.printstart "DB Preloading data"
Ionio.printreturn(3)

#============================================================================
# Table: Users
#============================================================================

if User.empty?    
  Ionio.printstart " inserting: Admin User"
    User.create  :login => 'sysop',
      :islocked => false,
      :is_admin => true,
      :password => 'changeme',
      :password_confirmation => 'changeme',
      :firstname => 'System',
      :lastname => 'Operator',
      :city => 'LeafScale',
      :state => 'CO',
      :country => 'USA',
      :pwexpires => Time.now + 90.days,
      :created => Time.now,
      :login_last => Time.now,
      :login_failures => 0,
      :login_total => 0,
      :pwhint_question => "What is the meaning of life?",
      :pwhint_answer => "42"

  Ionio.printreturn(0)
end


#============================================================================
# Table: Announcements
#============================================================================
if Announcement.empty?
  Ionio.printstart " inserting: Announcement"
    Announcement.create  :title => 'Welcome to the IonApp Framework',
      :submitted_by => 'Chris Tusa',
      :priority => 0,
      :created => Time.now,
      :expires => false,
      :expiration => nil,
      :body => <<eol
Congratulations!  You have successfully installed the IonApp Framework. 
There is a lot more to be done to create a fully working application. 
This message is stored as announcement in the database.
 
Sincerely,
Chris Tusa
eol
  Ionio.printreturn(0)
end
#'#fixes a display error in VS Code

#============================================================================
# DONE 
#============================================================================
Ionio.printstart "DB Preloading data"
Ionio.printreturn(0)
# 
