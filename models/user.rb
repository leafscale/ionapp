=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /models/user.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: This file holds the default data for preloading the database
#  tables needed to run a minimal install.
#
#-----------------------------------------------------------------------------

=begin rdoc               
= Users model
The Sequel model for the Users table
=end

class User < Sequel::Model(:users)
  attr_accessor :password, :password_confirmation
  
  def validate
    super
    validates_presence :password
    validates_length_range 4..40, :password
    validates_presence :password_confirmation
    errors.add(:password_confirmation, "must confirm password") if password != password_confirmation
  end

  def before_save
    super
    encrypt_password
  end

  def self.authenticate(login, password)
    user = filter(Sequel.function(:lower, :login) => Sequel.function(:lower, login)).first
    user && user.has_password?(password) ? user : nil
  end

  def has_password?(password)
    BCrypt::Password.new(self.password_hash) == password
  end

  # Returns a user by searched username
  def self.find_by_login(login)
    user = filter(Sequel.function(:lower, :login) => Sequel.function(:lower, login)).first
  end
  
  
private

  def encrypt_password
    self.password_hash = BCrypt::Password.create(password)
  end
end
