=begin
#
#===============================================================================
#   IonApp Framework
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /models/announcement.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Announcement DB model

#-----------------------------------------------------------------------------

=begin rdoc               
= Announcement model
The Sequel model for the Announcements table
=end

class Announcement < Sequel::Model(:announcements)

  def self.titleshash
    self.order(:priority).select_hash(:id, :title)
  end

end
