#!/bin/bash
# Load the runtime (JRUBY + JDK)
source ./runtime.sh
source ./session.sh
#
#===============================================================================
#                IonApp
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /start.sh
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: This is the startup script that starts the IonApp server. Using
#              rackup will not set the session information nor tune the 
#              JRuby and JVM. This file is called by systemd
#-----------------------------------------------------------------------------
#
#===============================================================================
#
#
#---[ Options ]---------------------------------------------------------------
#
#
# The following options include significant performance optimizations on the JVM
JRUBY_OPTS1="-Xcompile.invokedynamic=true -Xfixnum.cache=false"
# Turns on Graal VM
JRUBY_OPTS2="-J-XX:+UnlockExperimentalVMOptions -J-XX:+EnableJVMCI -J-XX:+UseJVMCICompiler"
# Set Puma's listen method
PUMA_LISTEN="tcp://127.0.0.1:9292"
# Location of the Access Log
PUMA_LOG_ACCESS="$IONAPP_HOME/log/access.log"
# Location of the Error Log
PUMA_LOG_ERRORS="$IONAPP_HOME/log/error.log"
# Set the environment 'production' or 'development' - should be set in runtime
# PUMA_ENV="production"



#----------------------------------------------------------------------------#
#                                                                            #
##==========================================================================##
#### DO NOT CHANGE ANYTHING BELOW THIS LINE ##################################
##==========================================================================##                                                                          ##
#                                                                            #
#----------------------------------------------------------------------------#
case "$PUMA_ENV" in
  "production")
    PUMA_D="-d"
  ;;
  *)
    PUMA_D=""
  ;;
esac

cd $IONAPP_HOME
$JRUBY_HOME/bin/jruby $JRUBY_OPTS1 $JRUBY_OPTS2 -W0\
 $JRUBY_HOME/bin/puma -e $PUMA_ENV -b $PUMA_LISTEN $PUMA_D \
   --redirect-stdout $PUMA_LOG_ACCESS \
   --redirect-stderr $PUMA_LOG_ERRORS \
   $IONAPP_HOME/config.ru